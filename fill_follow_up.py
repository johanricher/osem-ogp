#! /usr/bin/env python3


import argparse
import collections
import csv
import os
import re
import sys


generated_csv_names = (
    'id_link',
    'group',
    'track',
    'event_type_id_and_title',
    'language',
    'submitter.name',
    'submitter.email',
    'submitter.affiliation',
    'title',
    'rating',
    'comment1',
    'comment2',
    'comment3',
    'comment4',
    )
id_link_re = re.compile('=HYPERLINK\("(?P<url>.+?)", "(?P<id>\d+)"\)$')
proposal_csv_names = (
    'id_link',
    'group',
    'track',
    'event_type_id_and_title',
    'language',
    'submitter.name',
    'submitter.email',
    'submitter.affiliation',
    'title',
    )
selected_csv_names = (
    'id_link',
    'track',
    'event_type_id_and_title',
    'language',
    'submitter.name',
    'submitter.affiliation',
    'title',
    'rating',
    'comment1',
    'group',
    'comment2',
    'comment3',
    'comment4',
    )


parser = argparse.ArgumentParser()
# parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
args = parser.parse_args()

proposal_by_id = {}
with open('proposals-with-emails.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)
    labels = next(csv_reader)
    for row in csv_reader:
        assert all(not cell for cell in row[len(proposal_csv_names):]), 'Row is too long: {}'.format(row)
        proposal = collections.OrderedDict(
            (name, value)
            for name, value in zip(proposal_csv_names, row)
            if value
            )
        if proposal:
            match = id_link_re.match(proposal['id_link'])
            assert match is not None, proposal['id_link']
            id = int(match.group('id'))
            proposal_by_id[id] = proposal
print('Proposals: {}'.format(len(proposal_by_id)))

completed_event_by_id = {}
for filename_core in (
        'program-committee',
        'pitches',
        'subnational',
        'open-parliament',
        'etalab',
        ):
    print('Reading {}...'.format(filename_core))
    events = []
    with open(os.path.join('follow-up', '{}.csv'.format(filename_core)), 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            assert all(not cell for cell in row[len(selected_csv_names):]), 'Row is too long: {}'.format(row)
            event = collections.OrderedDict(
                (name, value)
                for name, value in zip(selected_csv_names, row)
                if value
                )
            if event:
                events.append(event)
    print(' ', len(events))

    for event in events:
        try:
            id = int(event['id_link'])
        except ValueError:
            proposal = None
        else:
            url = 'https://en.ogpsummit.org/osem/conference/ogp-summit/program/proposal/{}'.format(id)
            event['id_link'] = '=HYPERLINK("{}", "{}")'.format(url, id)
            proposal = proposal_by_id.pop(id, None)
            if proposal is None:
                if id in completed_event_by_id:
                    print('  Duplicate event:\n    {}\n    {}'.format(completed_event_by_id[id], event))
                else:
                    print('  Missing proposal for event:\n    {}'.format(event))
            else:
                event['submitter.email'] = proposal['submitter.email']
                completed_event_by_id[id] = event

    with open(os.path.join('follow-up', '{}-with-emails.csv'.format(filename_core)), 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(generated_csv_names)
        for event in events:
            writer.writerow([
                event[name] if event.get(name) is not None else ''
                for name in generated_csv_names
                ])

    with open(os.path.join('follow-up', 'rejected-with-emails.csv'), 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(proposal_csv_names)
        for id, proposal in sorted(proposal_by_id.items()):
            writer.writerow([
                proposal[name] if proposal.get(name) is not None else ''
                for name in proposal_csv_names
                ])
