#! /usr/bin/env python3


import argparse
from collections import OrderedDict
import copy
import csv
import json
import os
import re
import sys
import time

from mako import exceptions
from mako.template import Template
import mysql.connector
import requests


config = {
    'user': 'root',
    'password': '',
    'host': '127.0.0.1',
    'database': 'osem_prod',
    'raise_on_warnings': True,
    # 'use_pure': False,
    }

csv_names = (
    'id_link',
    'track',
    'event_type_id_and_title',
    'language',
    'submitter.name',
    'submitter.affiliation',
    'title',
    'non_admin_rating',
    'admin_rating',
    )

csv_labels0 = (
    'ID',
    'Track',
    'Type',
    'Language',
    'Submitter',
    'Affiliation',
    'Title',
    'Interest / 5',
    '',
    )

csv_labels1 = (
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'Public',
    'Committee',
    )

entry_template = Template("""\
£ [${entry['title']}](${entry['url']})

% if entry.get('subtitle'):
££ ${entry['subtitle']}
% endif

__Group__: ${entry['group.id']} - ${entry['group.title']}

__Track__: ${entry.get('track_id', 0)} - ${entry.get('track')}

__Type__: ${entry['event_type']}

__Language__: ${entry['language']}

__Committee Rating__: ${'{:0.2f}'.format(entry['admin_rating'])} / 5 (${entry['admin_rating_count']} votes)

__Public Rating__: ${'{:0.2f}'.format(entry['non_admin_rating'])} / 5 (${entry['non_admin_rating_count']} votes)

% if entry.get('abstract'):
£££ Abstract

${entry['abstract']}
% endif

% if entry.get('description'):
£££ Description

${entry['description']}
% endif

% if entry.get('objective'):
£££ Objective

${entry['objective']}
% endif

% for user in entry['users']:
£££ [${user['event_role']} - ${user.get('name', 'Anonymous')}](${user['url']})

    % if user.get('nickname'):
__Nickname__:  ${user['nickname']}
    % endif

    % if user.get('email'):
__Email__:  ${user['email']}
    % endif

    % if user.get('affiliation'):
__Affiliation__:  ${user['affiliation']}
    % endif

    % if user.get('biography'):
££££ Biography

${user['biography']}
    % endif

% endfor

£££ Other Informations

__Creation Date__: ${entry['created_at']}

__Update Date__: ${entry['updated_at']}

__Expected Audience__: ${entry['expected_audience']}

__Contact Phone Number__: ${entry['contact_phone_number']}

% if comments:
£££ Comments

    % for comment in comments:
££££ Comment by ${comment['display_username'] or comment['name'] or comment['username'] or 'Anonymous'}

${comment['raw']}

    % endfor
% endif
""")

event_names = (
    'id',
    'guid',
    'event_type_id',
    'title',
    'subtitle',
    'time_slots',
    'state',
    'progress',
    'language',
    'start_time',
    'abstract',
    'description',
    'public',
    'logo_file_name',
    'logo_content_type',
    'logo_file_size',
    'logo_updated_at',
    'proposal_additional_speakers',
    'track_id',
    'room_id',
    'created_at',
    'updated_at',
    'require_registration',
    'difficulty_level_id',
    'week',
    'is_highlight',
    'program_id',
    'expected_audience',
    'objective',
    'contact_phone_number',
    )

event_type_names = (
    'id',
    'title',
    'length',
    'minimum_abstract_length',
    'maximum_abstract_length',
    'color',
    'description',
    'program_id',
    'minimum_objective_length',
    'maximum_objective_length',
    )

event_type_template = Template("""\
£ ${event_type['title']}

% for entry in entries:
* [${entry['id']} - ${entry['language']} - ${entry['title']}](event-${entry['id']}.md)
% endfor
""")

event_user_names = (
    'id',
    'user_id',
    'event_id',
    'event_role',
    'comment',
    'created_at',
    'updated_at',
    )

group_id_by_track_id = {
    None: 3,
    3: 3,  # Climate and sustainable development
    4: 1,  # Transparency, accountability and fight against corruption
    5: 3,  # Digital and development
    6: 2,  # Civic tech and participatory tools
    7: 2,  # Open data and Open resources
    8: 3,  # Public innovation
    9: 4,  # Open government for cities
    10: 4,  # Open parliament
    11: 1,  # Access to information
    12: 4,  # Fundamentals liberties and human rights
    14: 3,  # Regional focus and Francophonie
    15: 4,  # Implementation of open government
    }

group_title_by_id = {
    1: "Public and economic sector transparency",
    2: "Digital",
    3: "Climate change and sustainable development",
    4: "Implementation of open government and new actors",
    }

osem_event_url_re = re.compile(
    r'https://(en\.|es\.|fr\.)?ogpsummit\.org/osem/conference/ogp-summit/program/proposal/(?P<id>\d+)$')

readme_template = Template("""\
£ OGP Summit Proposed Tracks

££ Group ${group_id} - ${group_title_by_id[group_id]}
""")

summary_template = Template("""\
£ OGP Summit Proposed Tracks

££ Group ${group_id} - ${group_title_by_id[group_id]}

<%
event_types = sorted(event_type_by_id.values(), key = lambda event_type: event_type['id'])
%>
% for event_type in event_types:
<%
    event_type_entries = [
        entry
        for entry in entries
        if entry['event_type_id'] == event_type['id']
        ]
%>
    % if event_type_entries:
* [${event_type['title']}](event-type-${event_type['id']}.md)
        % for entry in event_type_entries:
  - [${entry['id']} - ${entry['language']} - ${entry['title']}](event-${entry['id']}.md)
        % endfor
    % endif
% endfor
""")

topic_template = Template("""\
% if entry.get('subtitle'):
££ ${entry['subtitle']}
% endif

__Group__: ${entry['group.id']} - ${entry['group.title']}

__Track__: ${entry.get('track_id', 0)} - ${entry.get('track')}

__Type__: ${entry['event_type']}

__Language__: ${entry['language']}

__Committee Rating__: ${'{:0.2f}'.format(entry['admin_rating'])} / 5 (${entry['admin_rating_count']} votes)

__Public Rating__: ${'{:0.2f}'.format(entry['non_admin_rating'])} / 5 (${entry['non_admin_rating_count']} votes)

% if entry.get('abstract'):
£££ Abstract

${entry['abstract']}
% endif

% if entry.get('description'):
£££ Description

${entry['description']}
% endif

% if entry.get('objective'):
£££ Objective

${entry['objective']}
% endif

% for user in entry['users']:
£££ [${user['event_role']} - ${user.get('name', 'Anonymous')}](${user['url']})

    % if user.get('nickname'):
__Nickname__:  ${user['nickname']}
    % endif

    % if user.get('affiliation'):
__Affiliation__:  ${user['affiliation']}
    % endif

    % if user.get('biography'):
££££ Biography

${user['biography']}
    % endif

% endfor

£££ Other Informations

__Creation Date__: ${entry['created_at']}

__Update Date__: ${entry['updated_at']}

__Expected Audience__: ${entry['expected_audience']}

<hr>
<small>Il s'agit d'un sujet en provenance de l'article <a href="${entry['short_url']}">${entry['short_url']}</a></small>
""")

track_names = (
    'id',
    'guid',
    'name',
    'description',
    'color',
    'created_at',
    'updated_at',
    'program_id',
    )

user_names = (
    'id',
    'email',
    'encrypted_password',
    'reset_password_token',
    'reset_password_sent_at',
    'remember_created_at',
    'sign_in_count',
    'current_sign_in_at',
    'last_sign_in_at',
    'current_sign_in_ip',
    'last_sign_in_ip',
    'confirmation_token',
    'confirmed_at',
    'confirmation_sent_at',
    'unconfirmed_email',
    'created_at',
    'updated_at',
    'name',
    'email_public',
    'biography',
    'nickname',
    'affiliation',
    'avatar_file_name',
    'avatar_content_type',
    'avatar_file_size',
    'avatar_updated_at',
    'mobile',
    'tshirt',
    'languages',
    'volunteer_experience',
    'is_admin',
    'username',
    'is_disabled',
    )

vote_names = (
    'id',
    'event_id',
    'rating',
    'created_at',
    'updated_at',
    'user_id',
    )


parser = argparse.ArgumentParser()
parser.add_argument('-b', '--books', action = 'store_true', default = False,
    help = "Generate books")
parser.add_argument('-f', '--forum', action = 'store_true', default = False,
    help = "Update forum topics with OSEM data")
parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
args = parser.parse_args()


connection = mysql.connector.connect(**config)
cursor = connection.cursor()


query = ("""\
    SELECT * FROM event_types
    """)
cursor.execute(query)
event_type_by_id = {
    event_type['id']: event_type
    for event_type in (
        OrderedDict(
            (name, value)
            for name, value in zip(event_type_names, row)
            if value is not None
            )
        for row in cursor
        )
    }
print('event_type_by_id = {}'.format(len(event_type_by_id)))

query = ("""\
    SELECT * FROM event_users
    """)
cursor.execute(query)
event_users_by_event_id = {}
for event_user in (
        OrderedDict(
            (name, value)
            for name, value in zip(event_user_names, row)
            if value is not None
            )
        for row in cursor
        ):
    event_users_by_event_id.setdefault(event_user['event_id'], []).append(event_user)
print('event_users_by_event_id = {}'.format(len(event_users_by_event_id)))

query = ("""\
    SELECT * FROM events where state = 'new'
    """)
cursor.execute(query)
events = [
    OrderedDict(
        (name, value)
        for name, value in zip(event_names, row)
        if value is not None
        )
    for row in cursor
    ]
print('events = {}'.format(len(events)))

query = ("""\
    SELECT * FROM tracks
    """)
cursor.execute(query)
track_by_id = {
    track['id']: track
    for track in (
        OrderedDict(
            (name, value)
            for name, value in zip(track_names, row)
            if value is not None
            )
        for row in cursor
        )
    }
print('track_by_id = {}'.format(len(track_by_id)))

query = ("""\
    SELECT * FROM users
    """)
cursor.execute(query)
user_by_id = {
    user['id']: user
    for user in (
        OrderedDict(
            (name, value)
            for name, value in zip(user_names, row)
            if value is not None
            )
        for row in cursor
        )
    }
print('user_by_id = {}'.format(len(user_by_id)))

query = ("""\
    SELECT * FROM votes
    """)
cursor.execute(query)
votes_by_event_id = {}
for vote in (
        OrderedDict(
            (name, value)
            for name, value in zip(vote_names, row)
            if value is not None
            )
        for row in cursor
        ):
    votes_by_event_id.setdefault(vote['event_id'], []).append(vote)
print('votes_by_event_id = {}'.format(len(votes_by_event_id)))

cursor.close()
connection.close()

field_names = [
    {
        'event_type_id': 'event_type',
        'id': 'url',
        'track_id': 'track',
        }.get(name, name)
    for name in event_names
    if name not in (
        'guid',
        'state',
        'progress',
        'public',
        'require_registration',
        'week',
        'is_highlight',
        'program_id',
        )
    ] + [
    'group.id',
    'group.title',
    'user.event_role',
    ] + [
    'user.{}'.format({
        'id': 'url',
        }.get(name, name))
    for name in user_names
    if name not in (
        'created_at',
        'updated_at',
        'encrypted_password',
        'reset_password_token',
        'reset_password_sent_at',
        'remember_created_at',
        'sign_in_count',
        'current_sign_in_at',
        'last_sign_in_at',
        'current_sign_in_ip',
        'last_sign_in_ip',
        'confirmation_token',
        'confirmed_at',
        'confirmation_sent_at',
        'unconfirmed_email',
        'avatar_file_name',
        'avatar_content_type',
        'avatar_file_size',
        'avatar_updated_at',
        'mobile',
        'tshirt',
        'languages',
        'volunteer_experience',
        'is_admin',
        'username',
        'is_disabled',
        )
    ] + [
    'admin_rating_count',
    'admin_rating',
    'non_admin_rating_count',
    'non_admin_rating',
    ]
entries = []
for event in events:
    event_id = event['id']
    entry = {}
    entry.update(event)
    entry['event_type'] = event_type_by_id[entry['event_type_id']]['title']
    entry['event_type_id_and_title'] = '{} - {}'.format(entry['event_type_id'], entry['event_type'])
    track_id = entry.get('track_id')
    entry['group.id'] = group_id = group_id_by_track_id[track_id]
    entry['group.title'] = group_title_by_id[group_id]
    if track_id is not None:
        entry['track'] = track_by_id[track_id]['name']
    entry['short_url'] = 'https://ogpsummit.org/osem/conference/ogp-summit/program/proposal/{}'.format(event_id)
    entry['title'] = ' '.join(entry['title'].split()).strip().rstrip('.')
    entry['url'] = event_url = 'https://en.ogpsummit.org/osem/conference/ogp-summit/program/proposal/{}'.format(
        event_id)
    entry['id_link'] = '=HYPERLINK("{}", "{}")'.format(event_url, event_id)

    event_roles = []
    users = []
    for event_user in event_users_by_event_id[event_id]:
        user = user_by_id[event_user['user_id']]
        if user not in users:
            event_roles.append(event_user['event_role'])
            users.append(user)
    submitter = None
    users = copy.deepcopy(users)
    for user, event_role in zip(users, event_roles):
        user['event_role'] = event_role
        if submitter is None and event_role == 'submitter':
            submitter = user
        user['url'] = 'https://fr.ogpsummit.org/osem/users/{}'.format(user.pop('id'))
    assert submitter is not None
    entry['submitter'] = submitter
    entry['users'] = users
    merged_user_names = [
        {
            'id': 'url',
            }.get(name, name)
        for name in user_names
        ]
    for name in merged_user_names:
        if submitter.get(name) is not None:
            entry['submitter.{}'.format(name)] = submitter[name]
    merged_user = OrderedDict(
        ('user.{}'.format(merged_name), merged_value)
        for merged_name, merged_value in (
            (
                name,
                '\n'.join(
                    str(value) if value is not None else ''
                    for value in (
                        user.get(name)
                        for user in users
                        )
                    ).rstrip(),
                )
            for name in merged_user_names
            )
        if merged_value
        )
    merged_user['user.event_role'] = '\n'.join(event_roles)
    entry.update(
        (name, value)
        for name, value in merged_user.items()
        if name in field_names
        )

    admin_ratings = []
    non_admin_ratings = []
    for vote in votes_by_event_id.get(event_id, []):
        user = user_by_id[vote['user_id']]
        if user['is_admin']:
            admin_ratings.append(vote['rating'])
        else:
            non_admin_ratings.append(vote['rating'])
    entry['admin_rating'] = sum(admin_ratings) / len(admin_ratings) if admin_ratings else 0
    entry['admin_rating_count'] = len(admin_ratings)
    entry['non_admin_rating'] = sum(non_admin_ratings) / len(non_admin_ratings) if non_admin_ratings else 0
    entry['non_admin_rating_count'] = len(non_admin_ratings)

    entries.append(entry)

for index, name in reversed(list(enumerate(field_names[:]))):
    for entry in entries:
        if entry.get(name) not in (None, ''):
            break
    else:
        del field_names[index]

entries.sort(key = lambda entry: (
    entry['group.id'],
    entry['event_type_id'],
    -entry['admin_rating'],
    -entry['non_admin_rating'],
    entry['id'],
    ))


# Generate CSV files.


for group_id in group_title_by_id.keys():
    group_entries = [
        entry
        for entry in entries
        if entry['group.id'] == group_id
        ]
    with open('proposals-group-{}.csv'.format(group_id), 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(csv_labels0)
        writer.writerow(csv_labels1)
        for entry in group_entries:
            writer.writerow([
                entry[name] if entry.get(name) is not None else ''
                for name in csv_names
                ])


# Update forum topics and retrieve comments.


if not args.books and not args.forum:
    sys.exit(0)

comments_by_event_id = {}
entry_by_id = {
    entry['id']: entry
    for entry in entries
    }
page = 0
while True:
    print("Retrieving page {} of forum topics...".format(page))
    while True:
        response = requests.get('https://forum.etalab.gouv.fr/c/28/24.json', params=dict(page = page))
        if response.status_code == 429:  # Too Many Request
            print("  Sleeping because too many requests...")
            time.sleep(10)
        else:
            response.raise_for_status()
            break
    body = response.json()
    topics = body['topic_list']['topics']
    if not topics:
        break
    for topic_summary in topics:
        while True:
            response = requests.get('https://forum.etalab.gouv.fr/t/{}.json'.format(topic_summary['id']))
            if response.status_code == 429:  # Too Many Request
                print("  Sleeping because too many requests...")
                time.sleep(10)
            else:
                response.raise_for_status()
                break
        topic = response.json()
        # print("  {}".format(topic['title']))
        posts = topic['post_stream']['posts']
        main_post_summary = posts[0]
        links = main_post_summary.get('link_counts')
        if not links:
            print("  Skipping topic without link: ".format(topic['title']))
            continue
        urls = [
            link['url']
            for link in links
            ]
        osem_url = urls[0]
        if urls[0] == "https://ogpsummit.org/osem/conference/ogp-summit":
            # Skipping topic about category.
            continue
        osem_urls = [
            url
            for url in urls
            if osem_event_url_re.match(url) is not None
            ]
        if not osem_urls:
            print('  Skipping topic {} "{}" with bad or missing OSEM event link: {} / comments: {}'.format(
                topic['id'], topic['title'].strip(), ', '.join(urls), len(posts) - 1))
            continue
        if len(osem_urls) > 1:
            print('  Skipping topic {} "{}" with too much OSEM event links: {} / comments: {}'.format(
                topic['id'], topic['title'].strip(), ', '.join(osem_urls), len(posts) - 1))
            continue
        osem_url = osem_urls[0]
        event_id = int(osem_event_url_re.match(osem_url).group('id'))
        entry = entry_by_id.get(event_id)
        if entry is None:
            print('  Skipping topic {} "{}" with missing OSEM event / comments: {}'.format(
                topic['id'], topic['title'].strip(), len(posts) - 1))
            continue

        if args.forum and topic['title'] != entry['title']:
            if len(entry['title']) < 15:
                print('  Can\'t title of topic {} "{}" to "{}" because its length < 15'.format(
                    topic['id'], topic['title'].strip(), entry['title']))
            else:
                print('  Updating title of topic {} "{}" to "{}"'.format(
                    topic['id'], topic['title'].strip(), entry['title']))
                while True:
                    response = requests.put('https://forum.etalab.gouv.fr/t/{}'.format(topic['id']),
                        params=dict(
                            api_key = '3c2452f65e8e0ada15333a3a3a0be54c0a9f0da4c4fa0770a43743c6899c87df',
                            api_username = 'system',
                            category_id = topic['category_id'],
                            title = entry['title'],
                            topic_id = topic['id'],
                            ),
                        )
                    if response.status_code == 429:  # Too Many Request
                        print("  Sleeping because too many requests...")
                        time.sleep(10)
                    else:
                        response.raise_for_status()
                        break

        try:
            topic_markdown = topic_template.render(
                entry = entry,
                ).replace('£', '#').replace('\u2028', '')
        except:
            print(exceptions.text_error_template().render())
            raise

        while True:
            response = requests.get('https://forum.etalab.gouv.fr/posts/{}.json'.format(main_post_summary['id']))
            if response.status_code == 429:  # Too Many Request
                print("  Sleeping because too many requests...")
                time.sleep(10)
            else:
                response.raise_for_status()
                break
        main_post = response.json()

        if args.forum and main_post['raw'].strip() != topic_markdown.strip():
            print('  Updating content of topic {} "{}"'.format(topic['id'], topic['title'].strip()))
            while True:
                response = requests.put('https://forum.etalab.gouv.fr/posts/{}'.format(main_post['id']),
                    data=json.dumps(dict(
                        raw = topic_markdown,
                        )),
                    headers={'content-type': 'application/json'},
                    params=dict(
                        api_key = '3c2452f65e8e0ada15333a3a3a0be54c0a9f0da4c4fa0770a43743c6899c87df',
                        api_username = 'system',
                        ),
                    )
                if response.status_code == 429:  # Too Many Request
                    print("  Sleeping because too many requests...")
                    time.sleep(10)
                else:
                    response.raise_for_status()
                    break

        for post_summary in posts[1:]:
            while True:
                response = requests.get('https://forum.etalab.gouv.fr/posts/{}.json'.format(post_summary['id']))
                if response.status_code == 429:  # Too Many Request
                    print("  Sleeping because too many requests...")
                    time.sleep(10)
                else:
                    response.raise_for_status()
                    break
            post = response.json()
            comments_by_event_id.setdefault(event_id, []).append(post)

    page += 1


# Generate GitBooks.


if not args.books:
    sys.exit(0)

for group_id in sorted(group_title_by_id.keys()):
    book_path = 'book-group-{}'.format(group_id)

    if not os.path.exists(book_path):
        os.mkdir(book_path)

    group_entries = [
        entry
        for entry in entries
        if entry['group.id'] == group_id
        ]
    try:
        summary_markdown = summary_template.render(
            entries = group_entries,
            event_type_by_id = event_type_by_id,
            group_id = group_id,
            group_title_by_id = group_title_by_id,
            track_by_id = track_by_id,
            ).replace('£', '#')
    except:
        print(exceptions.text_error_template().render())
        raise
    with open(os.path.join(book_path, 'SUMMARY.md'), 'w') as summary_file:
        summary_file.write(summary_markdown)

    try:
        readme_markdown = readme_template.render(
            group_id = group_id,
            group_title_by_id = group_title_by_id,
            ).replace('£', '#')
    except:
        print(exceptions.text_error_template().render())
        raise
    with open(os.path.join(book_path, 'README.md'), 'w') as readme_file:
        readme_file.write(readme_markdown)

    for event_type in event_type_by_id.values():
        event_type_entries = [
            entry
            for entry in group_entries
            if entry['event_type_id'] == event_type['id']
            ]
        if event_type_entries:
            try:
                event_type_markdown = event_type_template.render(
                    entries = event_type_entries,
                    event_type = event_type,
                    ).replace('£', '#')
            except:
                print(exceptions.text_error_template().render())
                raise
            with open(os.path.join(book_path, 'event-type-{}.md'.format(event_type['id'])), 'w') as event_type_file:
                event_type_file.write(event_type_markdown)

    for entry in group_entries:
        try:
            event_markdown = entry_template.render(
                comments = comments_by_event_id.get(entry['id']),
                entry = entry,
                ).replace('£', '#').replace('\u2028', '')
        except:
            print(exceptions.text_error_template().render())
            raise
        with open(os.path.join(book_path, 'event-{}.md'.format(entry['id'])), 'w') as event_file:
            event_file.write(event_markdown)
