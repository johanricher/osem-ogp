class ConferenceController < ApplicationController
  protect_from_forgery with: :null_session
  before_filter :respond_to_options
  load_and_authorize_resource find_by: :short_title
  load_resource :program, through: :conference, singleton: true, except: :index

  def index
    @current = Conference.where('end_date >= ?', Date.current).order('start_date ASC')
    @antiquated = @conferences - @current
  end

  def show; end


  def new_proposals
    @events = @conference.program.events.new_proposals
  end

  def schedule
    @rooms = @conference.venue.rooms if @conference.venue
    @events = @conference.program.events
    @dates = @conference.start_date..@conference.end_date

    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = @conference.start_date.strftime('%Y-%m-%d')
    end
  end
  
  def completeschedule
    @rooms = @conference.venue.rooms.order(:order) if @conference.venue
    @events = @conference.program.events.confirmed
    @dates = @conference.start_date..@conference.end_date

    @schedule_dates = Date.new(2016, 12, 8)..Date.new(2016, 12, 9)
    
    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = @conference.start_date.strftime('%Y-%m-%d')
    end
  end

  def eventlists
    @rooms = @conference.venue.rooms.order(:order) if @conference.venue
    @events = @conference.program.events
    @dates = @conference.start_date..@conference.end_date

    @schedule_dates = Date.new(2016, 12, 8)..Date.new(2016, 12, 9)
    
    if @dates == Date.current
      @today = Date.current.strftime('%Y-%m-%d')
    else
      @today = @conference.start_date.strftime('%Y-%m-%d')
    end
  end
  
  def gallery_photos
    @photos = @conference.photos
    render 'photos', formats: [:js]
  end

  private

  def respond_to_options
    respond_to do |format|
      format.html { head :ok }
    end if request.options?
  end
end
