class AddBirthCountry < ActiveRecord::Migration
  def change
    add_column :registrations, :nationality_code, :string
    add_column :registrations, :birthcountry_code, :string
  end
end
