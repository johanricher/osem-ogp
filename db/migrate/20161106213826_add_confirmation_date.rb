class AddConfirmationDate < ActiveRecord::Migration
  def change
    add_column :registrations, :confirmation_date, :datetime
  end
end
